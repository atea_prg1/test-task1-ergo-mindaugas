package com.example.demo.controller;

import com.example.demo.entity.Person;
import com.example.demo.jpa.repository.PersonRepository;
import com.example.demo.jpa.specification.PersonSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@CrossOrigin
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/persons")
    public List<Person> getAll() {
        return personRepository.findAll();
    }

    @PostMapping("/persons")
    public Person savePerson(@RequestBody Person person) {
        return personRepository.save(person);
    }

    @GetMapping("/persons/getById")
    public Person getById(@RequestParam(value = "id") Long id) {
        return personRepository.findById(id).orElseGet(null);
    }

    @GetMapping("/persons/getByDateOfBirth")
    public List<Person> getByDateOfBirth(@RequestParam(value = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return personRepository.findByDateOfBirth(date);
    }

    @PostMapping("/persons/search")
    public List<Person> search(@RequestBody Person person) {
        Specification<Person> spec = new PersonSpecification(person);
        return personRepository.findAll(spec);
    }

}