package com.example.demo.controller;

import com.example.demo.entity.Person;
import com.example.demo.helper.JsonTransform;
import com.example.demo.jpa.repository.PersonRepository;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(PersonController.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonRepository personRepository;

    @Test
    public void greetingShouldReturnDefaultMessage() throws Exception {
        Person input = new Person();
        input.setName("tadas");
        Person output = new Person();
        output.setName("tadas");
        output.setId(1l);
        Mockito.when(personRepository.save(ArgumentMatchers.any(Person.class))).thenReturn(output);

        this.mockMvc.perform(MockMvcRequestBuilders.post("/persons", input)
                .contentType(JsonTransform.APPLICATION_JSON_UTF8)
                .content(JsonTransform.convertObjectToJsonBytes(input))
        )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("id", CoreMatchers.is(1)))
        ;
    }
}
