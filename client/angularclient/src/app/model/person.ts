export class Person {
  id: String | undefined;
  name: String | undefined;
  surname: String | undefined;
  dateOfBirth: any | undefined;
  gender: String | undefined;
};
